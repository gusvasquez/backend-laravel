<?php

namespace App\Helpers;

use Firebase\FWT\JWT;
use Iluminate\Support\Facades\DB;
use App\Models\User;

class JwtAuth {

    public $key;

    public function __construct() {
        $this->key = 'esto_es_una_clave_super_secreta-99887766';
    }

    public function signup($email, $password, $getToken = null) {

        //comprobar si existe el usuario con las credenciales

        $user = User::where([
                    'email' => $email,
                    'password' => $password
                ])->first();  // first saca solo unicamente un objeto
        //comprobar si son correctas (Objeto)

        $signup = false; //siempre va a ser falsa tenemos que comprobar si es un objeto

        if (is_object($user)) { // se comprueba que es un objeto la variable user con la consulta email , password
            $signup = true;
        }

        //generar el token con los datos del usuario identificado


        if ($signup) {
            $token = array(
                'sub' => $user->id,
                'email' => $user->email,
                'name' => $user->name,
                'surname' => $user->surname,
                'description' => $user->description,
                'image' => $user->image,
                'iate' => time(),
                'exp' => time() + (7 * 24 * 60 * 60)
            );

            $jwt = \Firebase\JWT\JWT::encode($token, $this->key, 'HS256');
            $decode = \Firebase\JWT\JWT::decode($jwt, $this->key, ['HS256']);

            //devolver los datos decodificados o el token, funcion de un parametro
            if (is_null($getToken)) {
                $data = $jwt;
            } else {
                $data = $decode;
            }
        } else {
            $data = array(
                'status' => 'error',
                'message' => 'Login Incorrecto'
            );
        }

        return $data;
    }

    public function checToken($jwt, $getIdentity = false) {

        $auth = false;

        try {
            $jwt = str_replace('"', '', $jwt);
            $decode = \Firebase\JWT\JWT::decode($jwt, $this->key, ['HS256']);
        } catch (\UnexpectedValueException $ex) {
            $auth = false;
        } catch (\DomainException $e) {
            $auth = false;
        }

        if (!empty($decode) && is_object($decode) && isset($decode->sub)) {
            $auth = true;
        } else {
            $auth = false;
        }

        if ($getIdentity) {
            return $decode;
        }

        return $auth;
    }

}

?>
