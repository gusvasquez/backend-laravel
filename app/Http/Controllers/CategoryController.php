<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller {

    //cargar middleware a controladores
    public function __construct() {
        $this->middleware('api.auth', ['except' => ['index', 'show']]);
    }

    public function index() {
        //sacar todos las categorias con el metodo all()
        $category = Category::all();

        return response()->json([
                    'code' => 200,
                    'status' => 'success',
                    'categories' => $category
        ],200);
    }

    //consultar una categoria en especifico
    public function show($id) {

        $category = Category::find($id);

        if (is_object($category)) {
            $data = array(
                'code' => 200,
                'status' => 'success',
                'category' => $category
            );
        } else {
            $data = array(
                'code' => 404,
                'status' => 'error',
                'message' => 'No existe la categoria'
            );
        }

        return response()->json($data, $data['code']);
    }

    //guardar categoria
    public function store(Request $request) {

        // Recoger los datos por POST
        $json = $request->input('json', null);  // si no llega la variable que sea nula

        $params_array = json_decode($json, true); // true para que convierta el json en un array
        //validar si el array no esta vacio
        if (!empty($params_array)) {

            // Validar los datos
            $validate = Validator::make($params_array, [
                        'name' => 'required'
            ]);

            //validar la si validate es fallido
            if ($validate->fails()) {
                $data = array(
                    'code' => 400,
                    'status' => 'success',
                    'message' => 'No se a guardado la categoria'
                );
            } else {
                // Guardar categoria
                $category = new Category();
                $category->name = $params_array['name'];
                $category->save();

                $data = array(
                    'code' => 200,
                    'status' => 'success',
                    'category' => $category
                );
            }
        } else {
            // si no a enviado ninguna categoria
            $data = array(
                'code' => 404,
                'status' => 'Error',
                'message' => 'No ha enviado ninguna categoria'
            );
        }
        // Devolver el resultado
        return response()->json($data, $data['code']);
    }

   

    //actualizar la categoria
    public function update($id, Request $request) {
        //recoger los datos que llegan por POST
        $json = $request->input('json', null);
        $params_array = json_decode($json, true);

        if (!empty($params_array)) {
            //Validar los datos
            $validate = Validator::make($params_array, [
                        'name' => 'required'
            ]);

            //Quitar lo que no se va a actualizar
            unset($params_array['id']);  // Elimina esos campos si llegan en el array, el id
            unset($params_array['created_at']);   // Elimina esos campos si llegan en el array, el created_at
            //Actualizar el registro (Categoria)
            $category = Category::where('id', $id)->update($params_array);
            $data = array(
                'code' => 200,
                'status' => 'success',
                'category' => $params_array
            );
        } else {
            $data = array(
                'code' => 400,
                'status' => 'Error',
                'message' => 'No haz enviado ninguna categoria'
            );
        }

        //Devolver los datos
        return response()->json($data, $data['code']);
    }

}
