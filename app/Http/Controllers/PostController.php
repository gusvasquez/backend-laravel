<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Helpers\JwtAuth;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller {

    //cargar middleware a controladores
    public function __construct() {
        $this->middleware('api.auth', ['except' => ['index', 'show', 'getImage','getPostsByCategory','getPostsByUser']]);
    }

    public function index() {
        $posts = Post::all()->load('category'); //load sirve para la cargar la relacion para que aparezca el nombre y no el id

        return response()->json([
                    'code' => 200,
                    'status' => 'success',
                    'posts' => $posts
                        ], 200);
    }

    public function show($id) {

        $post = Post::find($id)->load('category')->load('user');

        if (is_object($post)) {
            $data = array(
                'code' => 200,
                'status' => 'success',
                'post' => $post
            );
        } else {
            $data = array(
                'code' => 404,
                'status' => 'error',
                'message' => 'La entrada no existe.'
            );
        }
        return response()->json($data, $data['code']);
    }

    public function store(Request $request) {

        //recoger los datos por posts
        $json = $request->input('json', null);
        $params = json_decode($json);
        $params_array = json_decode($json, true);

        if (!empty($params_array)) {

            //conseguir el usuario identificado
            $user = $this->getidentity($request);
            //validar los datos

            $validate = Validator::make($params_array, [
                        'title' => 'required',
                        'content' => 'required',
                        'category_id' => 'required',
                        'image' => 'required'
            ]);

            // Validar si el validator falla
            if ($validate->fails()) {
                $data = array(
                    'code' => 400,
                    'status' => 'Error',
                    'message' => 'Faltan datos'
                );
            } else {
                //guardar el post
                $post = new Post();
                $post->user_id = $user->sub;
                $post->title = $params_array['title'];
                $post->content = $params_array['content'];
                $post->category_id = $params_array['category_id'];
                $post->image = $params_array['image'];
                $post->save();

                $data = array(
                    'code' => 200,
                    'status' => 'success',
                    'post' => $post
                );
            }
        } else {
            // si no a enviado ningun post
            $data = array(
                'code' => 404,
                'status' => 'Error',
                'message' => 'Por favor enviar los datos correctamente'
            );
        }
        //devolver la respuesta
        return response()->json($data, $data['code']);
    }

    public function update($id, Request $request) {
        //recoger los datos por post
        $json = $request->input('json');
        $params_array = json_decode($json, true);

        // Datos para devolver
        $data = array(
            'code' => 400,
            'status' => 'error',
            'message' => 'Datos incorrectos'
        );

        if (!empty($params_array)) {
            //validar los datos
            $validate = Validator::make($params_array, [
                        'title' => 'required',
                        'content' => 'required',
                        'category_id' => 'required'
            ]);

            if ($validate->fails()) {
                $data['errors'] = $validate->errors();
                return response()->json($data, $data['code']);
            }



            //eliminar lo que no se va a actualizar
            unset($params_array['id']);
            unset($params_array['user_id']);
            unset($params_array['created_at']);
            unset($params_array['user']);

            //conseguir el usuario identificado para que solo ese usuario puede actualizar el post
            $user = $this->getidentity($request);

            // buscar el registro
            $post = Post::where('id', $id)
                    ->where('user_id', $user->sub)
                    ->first();

            if (!empty($post) && is_object($post)) {
                //actualizar  el registro en concreto
                $post->update($params_array);

                $data = array(
                    'code' => 200,
                    'status' => 'success',
                    'post' => $post,
                    'changes' => $params_array
                );
            }
        }
        //devolver la respuesta
        return response()->json($data, $data['code']);
    }

    public function destroy($id, Request $request) {
        // eliminar post de el usuario que lo creo
        //conseguir el usuario identificado
        $user = $this->getidentity($request);
        // conseguir si existe el registro, y que solo el usuario que lo creo lo pueda eliminar
        $post = Post::where('id', $id)->where('user_id', $user->sub)->first();

        if (is_object($post)) {
            //borrar el registro
            $post->delete();

            //devolver
            $data = array(
                'code' => 200,
                'status' => 'success',
                'post' => $post
            );
            // si no envia ningun dato
        } else {
            $data = array(
                'code' => 404,
                'status' => 'error',
                'post' => 'El Post no existe'
            );
        }
        return response()->json($data, $data['code']);
    }

    private function getidentity($request) {
        //conseguir el usuario identificado
        $jwtAuth = new JwtAuth();
        $token = $request->header('Authorization', null);
        $user = $jwtAuth->checToken($token, true);  // se pone true que devuelva el objeto decodifcado del usuario identificado

        return $user;
    }

    //metodo para subir imagenes

    public function upload(Request $request) {

        //recoger la imagen
        $image = $request->file('file0');

        //validar la imagen
        $validate = Validator::make($request->all(), [
                    'file0' => 'required|image|mimes:jpg,jpeg,png,gif'
        ]);

        //guardar la imagen
        if (!$image || $validate->fails()) {
            $data = array(
                'code' => 400,
                'status' => 'error',
                'mesage' => 'Error al subir la imagen'
            );
        } else {
            $image_name = time() . $image->getClientOriginalName();

            \Storage::disk('images')->put($image_name, \File::get($image));

            $data = array(
                'code' => 200,
                'status' => 'success',
                'image' => $image_name
            );
        }


        //devolver datos
        return response()->json($data, $data['code']);
    }

    //sacar una imagen del disco
    public function getImage($filename) {

        //comprobar si existe el fichero
        $isset = \Storage::disk('images')->exists($filename);

        if ($isset) {
            //conseguir la imagen
            $file = \Storage::disk('images')->get($filename);

            //devolver la imagen
            return new Response($file, 200);
        } else {
            $data = array(
                'code' => 404,
                'status' => 'error',
                'message' => 'La imagen no existe'
            );
        }

        //mostrar error
        return response()->json($data, $data['code']);
    }

    //sacar post por categoria

    public function getPostsByCategory($id) {

        $posts = Post::where('category_id', $id)->get();

        return response()->json([
                    'status' => 'success',
                    'posts' => $posts
                        ], 200);
    }

    //sacar los post por usuario
    public function getPostsByUser($id) {
        $posts = Post::where('user_id', $id)->get();

        return response()->json([
                    'status' => 'success',
                    'posts' => $posts
                        ], 200);
    }

}
