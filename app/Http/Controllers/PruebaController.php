<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;

class PruebaController extends Controller
{
    public function index(){
        $titulo ='Animales';
        $animales  =['perro', 'gato', 'tigre'];
        
        return view('pruebas.index',array(
            'titulo' => $titulo,
            'animales' => $animales
        ));
    }
    
    public function textOrm() {
        
        $post = Post::all();
        
        
        $categories = Category::all();
        
        foreach ($categories as $category){
            echo "<h1>".$category->name."</h1>";
            
            foreach ($category->posts as $pos){
            echo "<h3>".$pos->title."</h3>";
            echo "<span style='color:blue;'>{$pos->user->name} - {$pos->category->name}</span>";
            echo "<p>".$pos->content."</p>";
            
        }
        
        echo "<hr>";
            
        }
        
        
        die();
    }
}
