<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Http\Response;

class UserController extends Controller {

    public function pruebas(Request $request) {
        return "Accion de user controller";
    }

    public function registro(Request $request) {

        //recoger los datos recogidos por posts

        $json = $request->input('json', null);

        //decodificar los datos

        $parametros = json_decode($json); //objeto

        $params_array = json_decode($json, true); //array
        //validar datos

        if (!empty($parametros) && !empty($params_array)) {

            //limpiar datos adelante y atras
            $params_array = array_map('trim', $params_array);

            $validate = Validator::make($params_array, [
                        'name' => 'required|alpha',
                        'surname' => 'required|alpha',
                        'email' => 'required|email|unique:users', //comprobar si el usuario existe, si existe no das un error
                        'password' => 'required'
            ]);

            if ($validate->fails()) {
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'el usuario no se a creado',
                    'errors' => $validate->errors()
                );
            } else {
                //cifrar la contraseña, cost es la veces que va a cifrar la contraseña
                $pwd = hash('sha256', $parametros->password);

                $user = new User();
                $user->name = $params_array['name'];
                $user->surname = $params_array['surname'];
                $user->email = $params_array['email'];
                $user->password = $pwd;
                $user->role = "ROLE_USER";

                //guardar el usuario
                $user->save();

                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'el usuario se a creado correctamente',
                    'user' => $user
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'Los datos no son correctos'
            );
        }

        //cifrar la contraseña
        //comprobar si el usuario existe
        //crear usuario



        return response()->json($data, $data['code']);
    }

    public function login(Request $request) {

        $jwtauth = new \App\Helpers\JwtAuth();

        //RECIBIR DATOS POR POST

        $json = $request->input('json', false);  //se pone null por si no llegan datos entonces que sea nulo

        $params = json_decode($json);

        $params_array = json_decode($json, true);

        //VALIDAR ESOS DATOS

        $validate = Validator::make($params_array, [
                    'email' => 'required|email', //comprobar si el usuario existe, si existe no das un error
                    'password' => 'required'
        ]);

        if ($validate->fails()) {
            $signup = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'el usuario no se a podido identificar',
                'errors' => $validate->errors()
            );
        } else {
            // cifrar password
            $pwd = hash('sha256', $params->password);

            //devolver token o datos
            $signup = $jwtauth->signup($params->email, $pwd);
            if (!empty($params->gettoken)) {
                $signup = $jwtauth->signup($params->email, $pwd, true);
            }
        }


        return response()->json($signup, 200);
    }

    public function update(Request $request) {

        // comprobar si el ussuario esta identificado
        $token = $request->header('Authorization');
        $jwtAuth = new \App\Helpers\JwtAuth();
        $checToken = $jwtAuth->checToken($token);

        //recoger los datos por post
        $json = $request->input('json', null);
        $params_array = json_decode($json, true);

        if ($checToken && !empty($params_array)) {
            //sacar usuario identificado
            $user = $jwtAuth->checToken($token, true);

            //validar los datos
            $validate = Validator::make($params_array, [
                        'name' => 'required|alpha',
                        'surname' => 'required|alpha',
                        'email' => 'required|email|unique:users, ' . $user->sub, //comprobar si el usuario existe, si existe no das un error
            ]);

            //quitar los campos que no quiero actualizar

            unset($params_array['id']);
            unset($params_array['role']);
            unset($params_array['password']);
            unset($params_array['created_at']);
            unset($params_array['remember_token']);

            //actualizar datos bd

            $user_update = User::where('id', $user->sub)->update($params_array);

            //devolver un array
            $data = array(
                'code' => 200,
                'status' => 'success',
                'user' => $user,
                'changes' => $params_array
            );
        } else {
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'El usuario no esta identificado'
            );
        }

        return response()->json($data, $data['code']);
    }

    public function upload(Request $request) {

        //RECOGER DATOS DE LA PETICION

        $image = $request->file('file0');

        //validacion de imagen

        $validator = Validator::make($request->all(), [
                    'file0' => 'required|image|mimes:jpg,jpeg,png,gif'
        ]);

        //GUARDAR IMAGEN

        if (!$image || $validator->fails()) {
            $data = array(
                'code' => 400,
                'status' => 'error',
                'mesage' => 'error al subir imagen');
        } else {
            $image_name = time() . $image->getClientOriginalName();
            \Storage::disk('users')->put($image_name, \File::get($image));

            $data = array(
                'code' => 200,
                'status' => 'success',
                'image' => $image_name);
        }


        //DEVOLVER EL RESULTADO

        return response()->json($data, $data['code']);
    }

    // metodo para obtener la imagen como parametro el nombre de la imagen
    public function getImage($filename) {

        //comprobar si existe una imagen con ese nombre en la carpeta user
        $isset = \Storage::disk('users')->exists($filename);

        if ($isset) {
            $file = \Storage::disk('users')->get($filename);
            return new Response($file, 200);
        } else {
            $data = array(
                'code' => 404,
                'status' => 'error',
                'mesage' => 'la imagen no existe');

            return response()->json($data, $data['code']);
        }
    }

    // metodo para obtener los datos de un usuario
    public function detail($id) {

        $user = User::find($id);

        //si es un objeto el usuario
        if (is_object($user)) {

            $data = array(
                'code' => 200,
                'status' => 'success',
                'user' => $user
            );
        } else {
            $data = array(
                'code' => 404,
                'status' => 'error',
                'message' => 'El Usuario no existe'
            );
        }
        return response()->json($data, $data['code']);
    }
}
