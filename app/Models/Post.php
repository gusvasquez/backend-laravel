<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    
    protected $table='posts';


    protected $fillable=[
        'user_id','category_id','title','content','image'
    ];
    
        //relacion de muchos a uno user
    public function user(){
        return $this->belongsTo('App\Models\User','user_id');
    }
    
        //relacion de muchos a uno category
    public function category(){
        return $this->belongsTo('App\Models\Category', 'category_id');
    }
}
