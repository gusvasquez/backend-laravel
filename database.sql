CREATE DATABASE IF NOT EXISTS api_rest_laravel;

USE api_rest_laravel;

CREATE TABLE users(
id   int (255) auto_increment not null,
name VARCHAR(50) NOT NULL,
surname VARCHAR(100),
role    VARCHAR(20),
email   VARCHAR(255) NOT NULL,
password VARCHAR(255) NOT NULL,
description text,
image   VARCHAR(255),
created_at datetime DEFAULT NULL,
updated_at datetime DEFAULT NULL,
remember_tokend VARCHAR(255),
CONSTRAINT pk_users PRIMARY KEY(id)
)ENGINE=InnoDb;


CREATE TABLE categories(
id int(255) auto_increment not null,
name VARCHAR(50) NOT NULL,
created_at datetime DEFAULT NULL,
updated_at datetime DEFAULT NULL,
CONSTRAINT pk_categories PRIMARY KEY(id)
)ENGINE=InnoDb;


CREATE TABLE posts(
id int(255) auto_increment not null,
user_id int(255) not null,
category_id int(255) not null,
title varchar(255) not null,
content text not null,
image varchar(255),
created_at datetime default null,
updated_at datetime default null,
CONSTRAINT pk_posts PRIMARY KEY(id),
CONSTRAINT fk_posts_use FOREIGN KEY(user_id) REFERENCES users(id),
CONSTRAINT fk_posts_category FOREIGN KEY(category_id) REFERENCES categories(id)
)ENGINE=InnoDb;