<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PruebaController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CategoryController;
use App\Http\Middleware\ApiAuthMiddleware;

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/pruebas/{nombre?}', function ($nombre = null) {
    $texto = '<h2>Texto desde una ruta: </h2>';
    $texto .= $nombre;
    return view('pruebas', array(
'texto' => $texto
    ));
});

Route::get('animales', [PruebaController::class, 'index']);
Route::get('textOrm', [PruebaController::class, 'textOrm']);

//Rutas del api
//Rutas prueba
//Route::get('users', [App\Http\Controllers\UserController::class, 'pruebas']);
//Route::get('posts', [App\Http\Controllers\PostController::class, 'pruebas']);
//Route::get('categories', [App\Http\Controllers\CategoryController::class, 'pruebas']);
//ruta oficiales
//Ruta Usuarios
Route::post('/api/registro', [UserController::class, 'registro']);
Route::post('/api/login', [UserController::class, 'login']);
Route::put('/api/user/update', [UserController::class, 'update']);
Route::post('/api/user/upload', [UserController::class, 'upload'])->middleware(ApiAuthMiddleware::class);
Route::get('api/user/avatar/{filename}', [UserController::class, 'getImage']);
Route::get('api/user/detail/{id}', [UserController::class, 'detail']);

//Ruta controlador Categoria
Route::resource('api/category', CategoryController::class);
//Ruta de controlador de entradas
Route::resource('api/post', PostController::class);
Route::post('/api/post/upload', [PostController::class, 'upload'])->middleware(ApiAuthMiddleware::class);

Route::get('/api/post/image/{filename}', [PostController::class, 'getImage']);

Route::get('/api/post/category/{id}',[PostController::class, 'getPostsByCategory']);

Route::get('/api/post/user/{id}',[PostController::class, 'getPostsByUser']);
